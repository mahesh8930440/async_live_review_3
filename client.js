const fetch = require("node-fetch");

async function fetchingFiles(data1){
  for(let file of data1){
    
    if (file.isDir){
      const response=await fetch (`http://localhost:5765/${file.name}`);
      const filesData= await response.json();
      fetchingFiles(filesData.files);
    }
    else{
      console.log(file.name);
    }
  }
}

async function main() {
  const response = await fetch("http://localhost:5765");
  const data = await response.json();
  //console.log(data);
  fetchingFiles(data.items);
}
main();
